module.exports = {

  // Port on which to run the server
  port: 3000,

  // enable debugging
  debug: true,

  // Pages to process for the frontend
  pages: ['index', 'app/index', 'app/app', 'app/options', 'app/dashboard', 'app/upload'],
}