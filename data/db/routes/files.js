const express = require('express');
const fs = require('fs');
const tus = require('tus');
const path = require('path');
const File = require('./../models/file');
const marked = require('marked');
const create_dompurify = require('dompurify');
const upload = require('express-fileupload');
const { JSDOM } = require('jsdom');
const dompurify = create_dompurify(new JSDOM().window);
const router = express.Router();


/*router.use("/files", tus.createServer({
  directory: "./data/storage",
  maxFileSize: 1024 * 1024 * 5,
  complete: function(req, res, next) {
    console.log("File uploaded with the following metadata:", req.upload);
    res.status(200).send('done');
  }
}));*/

router.get('/', async (req, res) => {
  const files = await File.find().sort({ date: 'desc' });
  /*const stats = fs.statSync('./../data/storage/'+files.filename);
  const fileSizeInBytes = stats["size"];*/
  res.render('app/dashboard', { files: files});
});

router.get('/new', (req, res) => {
  res.render('app/newfile', { file: new File() });
});

router.get('/edit/:id', async (req, res) => {
  const file = await File.findById(req.params.id);
  res.render('app/editfile', { file: file });
});

router.post('/', async (req, res, next) => {
  req.file = new File();
  next();
}, saveFile('newfile'));

router.put('/:id', async (req, res, next) => {
  req.file = await File.findById(req.params.id);
  fs.unlink(`./data/storage/${req.file.filename}`, (err) => {
    if (err) {
      throw err;
    } else {
      console.log('Edited successfully.');
    }
  });
  next();
}, saveFile('editfile'));

router.get('/:id', async (req, res) => {
  const file = await File.findById(req.params.id);
  const filename = file.filename;
  const filevalue = file.filevalue;
  const markedvalue = dompurify.sanitize(marked(filevalue));
  //console.log(filename.endsWith('.md'));
  if (filename.endsWith('.md')) {
    res.render('app/previewfile', { filename: filename, filevalue: filevalue, marked: markedvalue });
  } else {
    res.render('app/previewfile', { filename: filename, filevalue: filevalue });
  }
});

router.get('/download/:id', async (req, res, next) => {
  const file = await File.findById(req.params.id);
  const filename = file.filename;
  try {
    res.download(`./data/storage/${filename}`);
  } catch (e) {
    res.send(e);
  }
});
router.use(upload());
router.post('/uploadfile', (req, res) => {
  /*if (req.files) {
    const file = req.files.filename;
    const filename = req.files.filename.name;
    file.mv('./data/storage/'+filename, function(err){
      if(err) throw err;
      const dbfile = new File({ filename: filename, filevalue: file.data });
      dbfile.save();
    });
    console.log('File uploaded');
    res.redirect('/');
  } else {
    res.status(404).send("We couldn't find this file");
  }*/
  tus.createServer({
    directory: "./data/storage",
	  maxFileSize: 1024 * 1024 * 1024,
	  complete: function(req, res, next) {
			console.log("File uploaded with the following metadata:", req.upload);
			res.send(200);
	  }
  });
});

router.delete('/:id', async (req, res) => {
  const file = await File.findById(req.params.id);
  const filename = file.filename;
  fs.unlink(`./data/storage/${filename}`, (err) => {
    if (err) {
      throw err;
    } else {
      console.log('Deleted successfully.');
    }
  });
  await File.findByIdAndDelete(req.params.id);
  res.redirect('/');
});
//const storage = './../../storage';
//router.use('/files', express.static(storage));
/*fs.readdir(dirPath, (err,files)=>{
  /*if(err) throw err;
  else{
    files.forEach(file => {
      router.get(`/${file}`, (req,res)=>{
        fs.readFile(__dirname + `/../../storage/${file}`, (err,data)=>{
          if(err) throw err;
          else{
            res.send(data);
          }
        });
      });
    });
  }*//*
if(err){
throw err;
}else{
files.forEach(file => {
fs.readFile(__dirname + `/../../storage/${file}`, (err,data)=>{
  if(err){
    throw err;
  }else{
    router.get(`/${file}`, (req,res)=>{
      res.send(data);
    });
  }
});
});
}
});*/

function saveFile(path) {
  return async (req, res) => {
    let file = req.file;
    file.filename = req.body.filename;
    file.filevalue = req.body.filevalue;
    try {
      file = file.save();
      res.redirect('/');
      fs.writeFile(`./data/storage/${file.filename}`, `${file.filevalue}`, (err) => {
        if (err) {
          console.log(err);
        } else {
          console.log('File successfully added!');
        }
      });
    } catch (e) {
      res.render(`app/${path}`, { file: file });
    }
  }
}


module.exports = router;