// commit daily to nightly
// commit weekly to weekly  
// commit monthly to monthly
// commit yearly to yearly
//
// date start 30th August 2020
//
// hey guys - codeteacher is starting a team at 
// https://coders.flarum.cloud/
// feel free to join.

console.log("[DEBUG] Importing Sqreen")
const Sqreen = require('sqreen');
const config = require('./config.js');

console.log("[DEBUG] Importing Morgan")
var morgan = require('morgan')

function debugout(value) {
  if (config.debug) {
    console.log("[DEBUG] " + value)
  }
}

debugout(`Importing Filesystem Modules...`)
const path = require("path");
const fs = require("fs");
const tus = require('tus');

debugout(`Importing Server...`)
const express = require("express");
const app = express();

debugout(`Importing Database...`)
const File = require('./data/db/models/file');
const files_router = require('./data/db/routes/files');
const apps_router = require('./download/routes/downloads');

const method_override = require('method-override');
const mongoose = require('mongoose');

debugout(`Importing rateLimit...`)
const rateLimit = require("express-rate-limit");

debugout(`Importing Socket...`)
const http = require('http').Server(app);
const io = require('socket.io')(http);

debugout(`Importing Badwords...`)
const bad = require('bad-words');
const filter = new bad();
// var hbs = exphbs.create({});
console.log();
debugout(`Setting up Database...`);

mongoose.connect(`${process.env.DB_LINK}`, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });

debugout(`Setting up Rendering...`);
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
debugout(`Setting up Views...`);
app.set('views', path.join(__dirname, 'views'));
app.use(method_override('_method'));
app.use(Sqreen.middleware);
app.use(express.json());
app.use(morgan('dev'));
app.use(express.urlencoded({ extended: true }));

debugout(`Setting up Static...`);
app.use(express.static('public'));
/**
 * router.use("/files", tus.createServer({
  directory: "./data/storage",
  maxFileSize: 1024 * 1024 * 5,
  complete: function(req, res, next) {
    console.log("File uploaded with the following metadata:", req.upload);
    res.status(200).send('done');
  }
}));
 */
app.use('/files', files_router);
app.use('/download', apps_router)
app.use(express.static("node_modules"));

debugout(`Setting up Secure Connect...`);
app.use((req, res, next) => {
  if (req.headers['x-forwarded-proto'] === 'http') {
    res.redirect(301, `https://${req.headers.host}/${req.url}`);
  }
  next();
});

let connection = mongoose.connection;
connection.once('open', () => {
  console.log('DATABASE CONNECTED');
});

connection.once('error', () => {
  console.log('DATABASE ERROR');
});
app.enable("trust proxy");

const limiter = rateLimit({
  windowMs: 15 * 60 * 1000,
  max: 1000
});
app.use(limiter);

debugout(`Setting up Pages...`);
app.use((req, res, next) => {
  const url = req.path.toString().split('?')[0];
  const allowed = ['/', '/download', '/app/index'];
  const userId = req.get('X-Replit-User-Id');
  const files = File.find().sort({ date: 'desc' });
  if (userId) {
    const credentials = {
      username: req.get('X-Replit-User-Name'),
      id: userId,
      file: files,
      dark: false,
    }
    res.locals.creds = credentials;
  }
  if (allowed.includes(url)) {
    next();
  }
  else {
    userId ? next() : res.redirect('/');
  }
});

app.get('/', async (req, res) => {
  const files = await File.find().sort({ date: 'desc' });
  res.render('home', res.locals.creds); // not here - app/app
  // if(req.get('X-Replit-User-Id').render('home'))
});

app.all('/logout', (req, res) => {
  res.set('Clear-Site-Data', `"cache", "cookies", "storage", "executionContexts"`);
  res.redirect('/');
});

app.get('/draw', (req, res) => {
  res.render('app/draw');
});

app.post('/files/uploadfile', (req, res) => {
  
});

for (const page of config.pages) {
  app.get(`/${page}`, (req, res) => {
    res.render(`${page}.html`, res.locals.creds);
    if (config.debug) {
      console.log(`[DEBUG] "${page}" was requested.`)
    }
  });
}

app.use((req, res, next) => res.status(404).sendFile('404.html', { root: './views/error/' }));

debugout(`Setting up Server...`)
console.log()

const port = process.env.PORT || config.port;
app.listen(port, () => {
  console.log('Express server listening on port', port);
});