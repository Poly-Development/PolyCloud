## Foxglove Development
### Volunteers needed
We are requiring developers who can use NodeJS, MongoDB, HTML, CSS and/or JS.

We also need Digital artists that can create SVG art for the pages - more will be briefing when you join

| Filename  | Location | Uploader |
|---------------------------------|
| File 1    | link     | User     |
| File 3    | link     | User2    |
| File 2    | link     | User3    |
| File 4    | link     | User2    |
| File 7    | link     | User     |
| File 12   | link     | User3    |
| File 3    | link     | User     |


say we want to look for files made by "User"...

| Filename  | Location | Uploader |
|---------------------------------|
| File 1    | link     | User <-- |
| File 3    | link     | User2    |
| File 2    | link     | User3    |
| File 4    | link     | User2    |
| File 7    | link     | User <-- |
| File 12   | link     | User3    |
| File 3    | link     | User <-- |

We look in the uploader section and get the rows...

rows 1, 5 and 7 are found.

We then return the values of columns 1 and 2 in those rows.

| Filename  | Location | Uploader |
|---------------------------------|
| File 1    | link     | User <-- |
| File 7    | link     | User <-- |
| File 3    | link     | User <-- |

but we don't need row 3...

| Filename  | Location |
|----------------------|
| File 1    | link     |
| File 7    | link     |
| File 3    | link     |