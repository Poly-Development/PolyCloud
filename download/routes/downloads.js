const express = require('express');
const router = express.Router();

router.get('/', (req,res)=>{
  // ...
  res.send('DOWNLOAD PAGE :)')
});

router.get('/desktop', (req,res)=>{
  // ...
  res.send('Desktop app is under development');
});

router.get('/android', (req,res)=>{
  // Do what do you want - ZDev1
  res.send('Andriod app will be soon');
});

router.get('/ios', (req,res)=>{
  // Do what do you want - ZDev1
  res.send('iOS app will be soon');
});

module.exports = router;