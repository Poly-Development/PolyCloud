const mongoose = require('mongoose');
const bad = require('bad-words');
const slugify = require('slugify');
const filter = new bad();
const fs = require('fs');

const file_schema = new mongoose.Schema({
  filename: {
    type: String,
    required: true,
    maxlength: 64
  },
  filevalue: {
    type: String,
    maxlength: Infinity
  },
  filesize: {
    type: String,
  }
});

file_schema.pre('validate', function(next) {
  if (this.filevalue) {
    this.filevalue = filter.clean(this.filevalue);
  }

  if(this.filesize){
    const stats = fs.statSync("./../data/storage/"+this.filename);
    this.filesize = stats['size'];
    
    console.log(this.filesize)
  }
  /*if(this.filename.endsWith('.png') || this.filename.endsWith('.jpg') || this.filename.endsWith('.jpeg')){
    // TODO: Do something
  }*/
  //if(this.filesize){
  //this.filesize = size;
  //}
  fs.writeFile(`./data/storage/${this.filename}`, `${this.filevalue}`, (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log('Created successfully!');
    }
  });
  next();
});


module.exports = mongoose.model('File', file_schema);